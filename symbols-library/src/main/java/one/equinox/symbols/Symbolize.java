package one.equinox.symbols;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that the class will be symbolized.
 * That means that a new class will be created that will contain static names of the attributes of the class.
 *
 * Use only in classes.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface Symbolize {
}

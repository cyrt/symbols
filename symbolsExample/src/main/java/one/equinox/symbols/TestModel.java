package one.equinox.symbols;


import com.sun.istack.internal.NotNull;
import one.equinox.symbols.hiearchy.TestChildModel;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.ArrayList;

@Symbolize
public class TestModel {
    int attribute10;

    int anotherAttribute;

    String thirdAttributeSample;

    ArrayList<TestChildModel> childs;

    @DontSymbolize
    String not_me;

    @NotNull
    @XmlAttribute(name = "attr")
    String annotatedChildAttribute;

    @TestAnnotations.Email
    String annotatedChildAttribute2;
}

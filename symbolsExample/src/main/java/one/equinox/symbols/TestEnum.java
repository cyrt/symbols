package one.equinox.symbols;

@Symbolize
public enum TestEnum {
    A,B,C,AA,NAME;

    public TestEnum alwaysA() {
        return A;
    }
}

package one.equinox.symbols;

public class TestNestedModel {

    @Symbolize
    public static class NestedModel {
        int nestedInt;
    }
}

package one.equinox.symbols.processor;


import one.equinox.symbols.DontSymbolize;
import one.equinox.symbols.model.AttributeModel;
import one.equinox.symbols.model.ClassModel;

import javax.annotation.processing.Messager;
import javax.lang.model.element.*;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.util.*;

/**
 * Extracts the information needed from the typeElements and creates a model to be used to generated the sources.
 */
public class ModelBuilder {
    private final Messager messager;
    Set<TypeElement> typeElementList = new HashSet<TypeElement>();
    Map<TypeElement, ClassModel> classModels;

    public ModelBuilder(Messager messager) {
        this.messager = messager;
    }

    /**
     * Adds a new TypeElement for the build
     *
     * @param typeElement
     */
    public void add(TypeElement typeElement) {
        typeElementList.add(typeElement);
        //We want also to generate symbols for the parent class, even if not Symbolize annotated.
        TypeElement superTypeElement = getSuperClassTypeElement(typeElement);
        if (superTypeElement != null) {
            add(superTypeElement);
        }
    }

    /**
     * Generates a list of ClassModels with the added TypeElements
     *
     * @return
     */
    public List<ClassModel> build() {
        classModels = new HashMap<TypeElement, ClassModel>();
        //Generate class Models
        for (TypeElement typeElement : typeElementList) {
            ClassModel classModel = buildClassModel(typeElement);
            if (null != classModel) {
                classModels.put(typeElement, classModel);
            }
        }
        //add super-Models to Models
        for (TypeElement typeElement : typeElementList) {
            ClassModel classModel = classModels.get(typeElement);
            TypeElement superTypeElement = getSuperClassTypeElement(typeElement);
            if (superTypeElement != null) {
                ClassModel superClassModel = classModels.get(superTypeElement);
                classModel.setSuperClassModel(superClassModel);
            }
        }

        return new ArrayList<ClassModel>(classModels.values());
    }

    /**
     * Returns the typeElement of the superclass or null if its Object.
     *
     * @param typeElement class
     * @return superclass
     */
    private TypeElement getSuperClassTypeElement(TypeElement typeElement) {
        if (!typeElement.getSuperclass().toString().equals(Object.class.getName())) {
            return (TypeElement) ((DeclaredType) typeElement.getSuperclass()).asElement();
        } else {
            return null;
        }
    }

    /**
     * Creates a ClassModel given a typeElement
     *
     * @param classElement origin
     * @return generated model
     */
    private ClassModel buildClassModel(TypeElement classElement) {
        if (Enum.class.getName().equals(classElement.getQualifiedName().toString())) {
            return null;
        }
        String modelClass = classElement.getSimpleName().toString();

        String nesting = "";
        Element enclosingElement = classElement.getEnclosingElement();
        while (!(enclosingElement instanceof PackageElement)) {
            nesting = nesting + "." + enclosingElement.getSimpleName().toString().toLowerCase();
            enclosingElement = enclosingElement.getEnclosingElement();
        }
        PackageElement packageElement = (PackageElement) enclosingElement;
        String packageName = packageElement.getQualifiedName().toString();


        //NOTE: we can get info with typeUtils.asElement(superClassTypeMirror).getEnclosedElements();
        ClassModel data = new ClassModel(packageName, nesting, modelClass);

        //Add attributes
        if (classElement.getKind() == ElementKind.CLASS) {
            messager.printMessage(Diagnostic.Kind.NOTE, packageName + " = " + modelClass);

            TypeMirror superClassTypeMirror = classElement.getSuperclass();
            String superClassName = superClassTypeMirror.toString();
            data.setSupperClassName(superClassName);
            for (Element fieldElement : classElement.getEnclosedElements()) {
                if (filterField(fieldElement)) {
                    String attributeName = fieldElement.getSimpleName().toString();
                    data.addAttribute(new AttributeModel(attributeName));
                    data.addType(fieldElement.asType());
                }
            }
        } else if (classElement.getKind() == ElementKind.ENUM) {
            for (Element fieldElement : classElement.getEnclosedElements()) {
                if (filterEnumConstant(fieldElement)) {
                    String attributeName = fieldElement.getSimpleName().toString();
                    data.addAttribute(new AttributeModel(attributeName));
                }
            }
        }

        return data;
    }

    private boolean filterField(Element fieldElement) {
        if (fieldElement.getKind() != ElementKind.FIELD) {
            return false;
        }
        if (fieldElement.getModifiers().contains(Modifier.STATIC)) {
            return false;
        }
        if (fieldElement.getAnnotation(DontSymbolize.class) != null) {
            return false;
        }
        return true;
    }

    private boolean filterEnumConstant(Element fieldElement) {
        if (fieldElement.getKind() != ElementKind.ENUM_CONSTANT) {
            return false;
        }
        if (fieldElement.getAnnotation(DontSymbolize.class) != null) {
            return false;
        }
        return true;
    }
}

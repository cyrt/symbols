package one.equinox.symbols.generation;

import one.equinox.symbols.model.ClassModel;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import java.io.*;
import java.net.URL;
import java.util.Properties;

/**
 * In charge of generating the new classes, given a model.
 */
public class ModelToJava2 {
    public static void create(ProcessingEnvironment processingEnv, ClassModel classModel) {
        try {
            final String javaSource = prepareJavaSource(classModel);

            if (!compareWithExisting(processingEnv, classModel, javaSource)) {
                writeJavaFile(processingEnv, classModel, javaSource);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static String prepareJavaSource(ClassModel classModel) throws IOException {
        Properties props = new Properties();
        URL url = ModelToJava2.class.getClassLoader().getResource("velocity.properties");
        props.load(url.openStream());

        VelocityEngine ve = new VelocityEngine(props);
        ve.init();

        VelocityContext vc = new VelocityContext();
        vc.put("data", classModel);

        Template vt = ve.getTemplate("SymbolClass.vm");

        final StringWriter stringWriter = new StringWriter();
        vt.merge(vc, stringWriter);

        return stringWriter.getBuffer().toString();
    }

    private static void writeJavaFile(ProcessingEnvironment processingEnv, ClassModel classModel, String javaSource) throws IOException {
        JavaFileObject jfo = processingEnv.getFiler().createSourceFile(classModel.getGeneratedFullName());

        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "--generating java file: " + jfo.toUri());
        Writer writer = new StringWriter();
        try {
            writer = jfo.openWriter();
            writer.write(javaSource);
        } finally {
            writer.close();
        }
    }

    private static boolean compareWithExisting(ProcessingEnvironment processingEnv, ClassModel classModel, String javaSource) throws IOException {
        final Messager messager = processingEnv.getMessager();
        final FileObject resource = processingEnv.getFiler().getResource(
                StandardLocation.SOURCE_OUTPUT,
                classModel.getGeneratedPackage(),
                classModel.getGeneratedClassName() + ".java");
        if (resource.getLastModified() < 1) {
            return false;
        }
        Reader reader = new StringReader("");
        try {
            reader = resource.openReader(true);
            char[] cbuf = new char[javaSource.length()];
            final int read = reader.read(cbuf);
            if (read == javaSource.length()) {
                final String current = new String(cbuf, 0, read);
                if (current.startsWith(javaSource)) {
                    messager.printMessage(Diagnostic.Kind.OTHER,
                            "--up to date: " + classModel.getGeneratedFullName());
                    return true;
                }
            }
        } catch (IOException e) {
            messager.printMessage(Diagnostic.Kind.OTHER, e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private ModelToJava2() {
        //no instance
    }
}

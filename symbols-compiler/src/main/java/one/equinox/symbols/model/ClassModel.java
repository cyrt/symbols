package one.equinox.symbols.model;

import org.apache.commons.lang.ArrayUtils;

import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVisitor;
import java.util.ArrayList;
import java.util.List;

/**
 * Model of the data of a class
 */
public class ClassModel {
    String originPackageName;
    private final String additionalNestingPackageName;
    String originModelClass;
    List<AttributeModel> attributes = new ArrayList<AttributeModel>();
    final List<String> types = new ArrayList<String>();
    String supperClassName;
    ClassModel superClassModel;

    public ClassModel(String originPackageName, String additionalNestingPackageName, String originModelClass) {
        this.originPackageName = originPackageName;
        this.additionalNestingPackageName = additionalNestingPackageName;
        this.originModelClass = originModelClass;
    }

    public ClassModel setSupperClassName(String supperClassName) {
        this.supperClassName = supperClassName;
        return this;
    }

    public void addAttribute(AttributeModel attribute) {
        attributes.add(attribute);
    }

    public void addType(TypeMirror type) {
        String[] names = type.toString()
                .split("<")[0]
                .replace(")", "").split("::");
        String name = names[names.length - 1];
        if (name.matches("^\\w+(\\.\\w+)*$")) {
            types.add(name + ".class");
        } else {
            types.add(Object.class.getName() + ".class");
        }
    }

    public String getGeneratedClassName() {
        return originModelClass + "Symbols";
    }

    public String getGeneratedPackage() {
        return originPackageName + ".symbols" + additionalNestingPackageName;
    }

    public String getGeneratedFullName() {
        return getGeneratedPackage() + "." + getGeneratedClassName();
    }

    public String getOriginFullName() {
        return getOriginPackageName() + "." + getOriginModelClass();
    }

    public String getOriginModelClass() {
        return originModelClass;
    }

    public List<AttributeModel> getOwnAttributes() {
        return attributes;
    }

    public List<AttributeModel> getAttributes() {
        List<AttributeModel> result = new ArrayList<AttributeModel>(getOwnAttributes());
        if (getSuperClassModel() != null) {
            result.addAll(getSuperClassModel().getAttributes());
        }
        return result;
    }

    public List<String> getTypes() {
        List<String> result = new ArrayList<String>(types);
        if (getSuperClassModel() != null) {
            result.addAll(getSuperClassModel().getTypes());
        }
        return result;
    }

    public String getOriginPackageName() {
        return originPackageName;
    }

    public String getOriginClassName() {
        return originModelClass;
    }

    public String getSupperClassName() {
        return supperClassName;
    }

    public ClassModel getSuperClassModel() {
        return superClassModel;
    }

    public void setSuperClassModel(ClassModel superClassModel) {
        this.superClassModel = superClassModel;
    }

}
